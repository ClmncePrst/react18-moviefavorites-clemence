import PropTypes from "prop-types";
import { useState } from "react";
import "./MovieItem.css";


function MovieItem(props) {
  const { title, released, director, poster } = props;

  // Utilisation du hook useState avec false comme valeur par défaut
const [favourite, setFavourite] = useState(false);
  // Fonction fléchée qui fait changer l'état de "favourite" quand on clique
const toggleFavourite = () => {
  setFavourite(!favourite);
}



  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>
      <button type="button" onClick={toggleFavourite}> {favourite ? "Remove from favourites" : "Add to favourites"} </button>
    </div>
  );
}

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
